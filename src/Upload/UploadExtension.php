<?php

namespace Solnet\Upload;

use SilverStripe\Core\Extension;

class UploadExtension extends Extension
{
    public function onAfterLoadIntoFile($file)
    {
        if ($file->exists()) {
            $file->doPublish();
        }
    }
}
